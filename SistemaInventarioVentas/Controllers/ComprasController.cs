﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SistemaInventarioVentas.Models;
using SistemaInventarioVentas.Models.viewmodels;
using Newtonsoft.Json;

namespace SistemaInventarioVentas.Controllers
{
    [Authorize]
    public class ComprasController : Controller
    {
        private SistemaInventarioDBContext db = new SistemaInventarioDBContext();

        // GET: Compras
        public ActionResult Index()
        {
            var compras = db.Compras
                .Include(c => c.Proveedor)
                .Include(c => c.DetallesCompras)
                .ToList();
            return View(compras);
        }

        // GET: Compras/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Compra compra = db.Compras.Find(id);
            if (compra == null)
            {
                return HttpNotFound();
            }
            return View(compra);
        }

        [Route("Compras/GetProveedores")]
        public ActionResult GetProveedores(string param)
        {
            if (string.IsNullOrWhiteSpace(param))
                return Json(db.Proveedors.ToList(), JsonRequestBehavior.AllowGet);

            List<Proveedor> proveedors = db.Proveedors
                    .Where(c => c.Nombre.Contains(param) || c.RNC.Contains(param))
                    .ToList();
            return Json(proveedors, JsonRequestBehavior.AllowGet);
        }

        [Route("Compras/GetProductos")]
        public ActionResult GetProductos(string param)
        {
            List<object> data = null;

            if (string.IsNullOrWhiteSpace(param))
            {
                List<Producto> p = db.Productos.ToList();
                data = ModearData(p);
                return Json(data, JsonRequestBehavior.AllowGet);
            }

            List<Producto> productos = db.Productos.Where(p => p.Estado == true && p.Cantidad > 0 &&
                     p.Nombre.ToLower().IndexOf(param.ToLower()) > +1).ToList();

            data = ModearData(productos);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private List<Object> ModearData(List<Producto> productos)
        {
            List<object> data = new List<object>();
            foreach (Producto producto in productos)
            {
                data.Add(new
                {
                    producto.ProductoID,
                    producto.Nombre
                });
            }
            return data;
        }

        [Route("Compras/GetProducto")]
        public ActionResult GetProducto(int id)
        {
            Producto producto = db.Productos.SingleOrDefault(p => p.ProductoID == id && p.Estado == true);
            if (producto == null)
                return Json("0", JsonRequestBehavior.AllowGet);
            else
                return Json(new
                {
                    producto.ProductoID,
                    producto.PrecioCompra,
                    producto.Cantidad,
                    producto.Nombre
                }, JsonRequestBehavior.AllowGet);
        }

        // GET: Compras/Create
        public ActionResult Create()
        {
            CompraViewModel compra = new CompraViewModel()
            {
                Productos = new List<Producto>(),
                Proveedores = new List<Proveedor>(),
                Comentario = ""
            };
            return View(compra);
        }

        // POST: Compras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Route("Compras/Create")]
        public ActionResult Create(string data)
        {
            GuardarCompraViewModel compra = JsonConvert.DeserializeObject<GuardarCompraViewModel>(data);
            Proveedor proveedor = db.Proveedors.Find(compra.ProveedorID);
            Producto producto = new Producto();
            Compra nuevaCompra = new Compra()
            {
                DetallesCompras = new List<DetalleCompra>()
            };
            List<DetalleCompra> detalles = new List<DetalleCompra>();

            nuevaCompra.Comentario = compra.Comentario;
            nuevaCompra.Estado = true;
            nuevaCompra.Proveedor = proveedor;
            nuevaCompra.FechaCompra = DateTime.UtcNow;
            nuevaCompra.Total = compra.Total;

            foreach (DetalleCompra detalleCompra in compra.Productos)
            {
                producto = db.Productos.Find(detalleCompra.ProductoID);
                producto.Cantidad += detalleCompra.Cantidad;
                nuevaCompra.DetallesCompras.Add(detalleCompra);
            }

            try
            {
                db.Compras.Add(nuevaCompra);
                db.SaveChanges();
                return Json(new { success = "1", id = nuevaCompra.CompraID });
            }
            catch (Exception)
            {
                return Json("0");
            }
        }

        //// GET: Compras/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Compra compra = db.Compras.Find(id);
        //    if (compra == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ProveedorID = new SelectList(db.Proveedors, "ProveedorID", "Nombre", compra.ProveedorID);
        //    return View(compra);
        //}

        // POST: Compras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "CompraID,FechaVenta,Total,Estado,Comentario,ProveedorID")] Compra compra)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(compra).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ProveedorID = new SelectList(db.Proveedors, "ProveedorID", "Nombre", compra.ProveedorID);
        //    return View(compra);
        //}

        //// GET: Compras/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Compra compra = db.Compras.Find(id);
        //    if (compra == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(compra);
        //}

        //// POST: Compras/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Compra compra = db.Compras.Find(id);
        //    db.Compras.Remove(compra);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
