// <auto-generated />
namespace SistemaInventarioVentas.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class compra : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(compra));
        
        string IMigrationMetadata.Id
        {
            get { return "201908061645483_compra"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
