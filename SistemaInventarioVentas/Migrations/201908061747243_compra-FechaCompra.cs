namespace SistemaInventarioVentas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class compraFechaCompra : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Compras", "FechaCompra", c => c.DateTime(nullable: false));
            DropColumn("dbo.Compras", "FechaVenta");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Compras", "FechaVenta", c => c.DateTime(nullable: false));
            DropColumn("dbo.Compras", "FechaCompra");
        }
    }
}
