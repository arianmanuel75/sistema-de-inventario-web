namespace SistemaInventarioVentas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Proveedores : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Proveedors",
                c => new
                    {
                        ProveedorID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                        RNC = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ProveedorID);
            
            AddColumn("dbo.Productos", "Proveedor_ProveedorID", c => c.Int());
            CreateIndex("dbo.Productos", "Proveedor_ProveedorID");
            AddForeignKey("dbo.Productos", "Proveedor_ProveedorID", "dbo.Proveedors", "ProveedorID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Productos", "Proveedor_ProveedorID", "dbo.Proveedors");
            DropIndex("dbo.Productos", new[] { "Proveedor_ProveedorID" });
            DropColumn("dbo.Productos", "Proveedor_ProveedorID");
            DropTable("dbo.Proveedors");
        }
    }
}
