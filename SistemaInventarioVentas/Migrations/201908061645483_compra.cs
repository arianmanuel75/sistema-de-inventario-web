namespace SistemaInventarioVentas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class compra : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DetalleCompras",
                c => new
                    {
                        CompraID = c.Int(nullable: false),
                        ProductoID = c.Int(nullable: false),
                        PrecioUnitario = c.Single(nullable: false),
                        SubTotal = c.Single(nullable: false),
                        Cantidad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CompraID, t.ProductoID })
                .ForeignKey("dbo.Compras", t => t.CompraID, cascadeDelete: true)
                .ForeignKey("dbo.Productos", t => t.ProductoID, cascadeDelete: true)
                .Index(t => t.CompraID)
                .Index(t => t.ProductoID);
            
            CreateTable(
                "dbo.Compras",
                c => new
                    {
                        CompraID = c.Int(nullable: false, identity: true),
                        FechaVenta = c.DateTime(nullable: false),
                        Total = c.Single(nullable: false),
                        Estado = c.Boolean(nullable: false),
                        Comentario = c.String(),
                        ProveedorID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CompraID)
                .ForeignKey("dbo.Proveedors", t => t.ProveedorID, cascadeDelete: true)
                .Index(t => t.ProveedorID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetalleCompras", "ProductoID", "dbo.Productos");
            DropForeignKey("dbo.Compras", "ProveedorID", "dbo.Proveedors");
            DropForeignKey("dbo.DetalleCompras", "CompraID", "dbo.Compras");
            DropIndex("dbo.Compras", new[] { "ProveedorID" });
            DropIndex("dbo.DetalleCompras", new[] { "ProductoID" });
            DropIndex("dbo.DetalleCompras", new[] { "CompraID" });
            DropTable("dbo.Compras");
            DropTable("dbo.DetalleCompras");
        }
    }
}
