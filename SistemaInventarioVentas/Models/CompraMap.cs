﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace SistemaInventarioVentas.Models
{
    public class CompraMap : EntityTypeConfiguration<Compra>
    {
        public CompraMap()
        {
            // Primary key
            this.HasKey(m => m.CompraID);

            this.Property(m => m.CompraID)
                .HasColumnType("int")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Properties
            this.Property(m => m.FechaCompra)
                .HasColumnType("datetime")
                .HasPrecision(0);

            this.Property(m => m.Total)
                .HasColumnType("float");

            this.Property(m => m.Estado)
                .HasColumnType("bool");

            this.Property(m => m.Comentario)
                .HasMaxLength(100);

            this.Property(m => m.ProveedorID)
                .HasColumnType("int")
                .IsRequired();
        }
    }
}