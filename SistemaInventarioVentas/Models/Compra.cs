﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaInventarioVentas.Models
{
    public class Compra
    {
        public int CompraID { get; set; }
        [Display(Name = "Fecha de compra")]
        public DateTime FechaCompra { get; set; }
        public float Total { get; set; }
        public bool Estado { get; set; }
        public string Comentario { get; set; }

        [Required(ErrorMessage ="El id del proveedor es obligatorio")]
        [Display(Name = "Nombre proveedor")]
        public int ProveedorID { get; set; }
        public Proveedor Proveedor { get; set; }
        public ICollection<DetalleCompra> DetallesCompras { get; set; }
    }
}