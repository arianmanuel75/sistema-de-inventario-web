﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaInventarioVentas.Models
{
    public class Almacen
    {
        public int AlmacenID { get; set; }

        [Required(ErrorMessage = "El campo nombre no puede ir vacio")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo codigo Almacen no puede ir vacio")]
        [Display(Name = "Codigo del Almacen")]
        [RegularExpression(@"AL-[A-Z]{3}", ErrorMessage = "El codigo debe comenzar con 'AL-' y tener 3 letras mayusculas")]
        public string CodigoAlmacen { get; set; }

        [JsonIgnore]
        public virtual ICollection<Producto> Productos { get; set; }
    }
}