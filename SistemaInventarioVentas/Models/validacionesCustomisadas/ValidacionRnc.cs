﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaInventarioVentas.Models.validacionesCustomisadas
{
    public class ValidacionRnc : ValidationAttribute
    {
        private readonly SistemaInventarioDBContext _context;
        public ValidacionRnc()
        {
            _context = new SistemaInventarioDBContext();
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var proveedor = (Proveedor)validationContext.ObjectInstance;
            var proveedorEnDB = _context.Proveedors.SingleOrDefault(c => c.RNC == proveedor.RNC);


            if (proveedorEnDB != null && proveedorEnDB.ProveedorID != proveedor.ProveedorID)
                return new ValidationResult("Ya existe un proveedor con este RNC");

            return ValidationResult.Success;
        }
    }
}