﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SistemaInventarioVentas.Models.validacionesCustomisadas;

namespace SistemaInventarioVentas.Models
{
    public class Proveedor
    {
        public int ProveedorID { get; set; }

        [Required(ErrorMessage = "Debe introducir el nombre de un proveedor")]
        [MinLength(3, ErrorMessage = "El nombre debe tener al menos 3 caracteres")]
        [MaxLength(20, ErrorMessage = "El nombre debe ser de 20 o menos caracteres")]
        public string Nombre { get; set; }

        [ValidacionRnc]
        [Required(ErrorMessage = "El campo RNC es obligatorio")]
        [RegularExpression(@"\b[145]{1}\d{2}\-?\d{5}\-?\d{1}\b", ErrorMessage = "el RNC no es valida")]
        public string RNC { get; set; }

        public ICollection<Producto> Productos { get; set; }
    }
}