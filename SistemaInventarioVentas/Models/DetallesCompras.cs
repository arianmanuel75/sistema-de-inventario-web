﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SistemaInventarioVentas.Models
{
    public class DetalleCompra
    {
        [Key, Column(Order = 1)]
        [Required(ErrorMessage = "El id del producto es obligatorio")]
        public int ProductoID { get; set; }
        public Producto Producto { get; set; }

        [Key, Column(Order = 0)]
        [Required(ErrorMessage = "El id de la compra es obligatorio")]
        public int CompraID { get; set; }
        public Compra Compra { get; set; }

        public float PrecioUnitario { get; set; }

        public float SubTotal { get; set; }

        public int Cantidad { get; set; }
    }
}